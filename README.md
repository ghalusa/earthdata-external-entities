# Earthdata External Entities

## Contents of this File

* Introduction
* Requirements
* Installation
* Maintainers


### Introduction

The Earthdata External Entities module makes it possible to connect to datasets from external databases and use them in your Drupal 8/9 website.


### Requirements

* N/A


### Installation

* Install as you would normally install a contributed Drupal module.
   See: [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for further information.


### Maintainers/Support

* Scott Brenner: [scott.brenner@ssaihq.com](mailto:scott.brenner@ssaihq.com)
* Goran Halusa: [goran.n.halusa@nasa.gov](mailto:goran.n.halusa@nasa.gov)
* SSAI: [https://www.ssaihq.com](https://www.ssaihq.com)